'use strict';

class Stack {
  constructor () {
    this.items = [];
    this.emptyMessage = 'Stack is empty';
  }

  push (item) {
    if (item !== null && item !== undefined) {
      return this.items.push(item);
    }
    return 'Invalid entry';
  }

  pop () {
    if (this.items.length > 0) {
      return this.items.pop();
    }
    return this.emptyMessage;
  }

  peek () {
    if (this.items.length > 0) {
      return this.items[this.items.length - 1];
    }
    return this.emptyMessage;
  }

  isEmpty () {
    return this.items.length === 0;
  }

  printStack () {
    return this.items.length > 1 ? this.items.reduce(function (sum, item) {
      return sum + item + ' ';
    }, []) : this.emptyMessage;
  }
}

let newStack = new Stack();

newStack.push(10);
newStack.push(20);
newStack.push(30);

console.log(newStack.isEmpty());

console.log(newStack.printStack());

console.log(newStack.peek());
