'use strict';

// Find min & max values
// Based off of Javascripts Prototype methods

let arr = [5,6,7];

const max = Math.max.apply(null, arr);
const min = Math.min.apply(null, arr);

console.log('max value: ' + max);
console.log('min value: ' + min);

const maxES6 = Math.max(...arr);
const minES6 = Math.min(...arr);

console.log(`max value es6 ${maxES6}`);
console.log(`min value es6 ${minES6}`);
