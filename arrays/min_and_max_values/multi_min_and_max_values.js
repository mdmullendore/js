'use strict';

// Find greatest/ nth (limit) numbers
// Step 1: remove duplicate values
// Step 2: sort array from greatest to lowests or vice versa
// Step 3: reduce array to limit
let array1 = [1, 3, 2, 4, 3, 2, 10];
let limit = 3;

let output = array1.filter(function(item, pos) {
    return array1.indexOf(item) == pos;
  })
  .sort(function (a, b) {
    return b - a;
  })
  .slice(0, limit);

console.log(output);

// arrow function example =>
let output2 = array1
  .filter((item, pos) => array1.indexOf(item) == pos)
  .sort((a, b) => b - a)
  .slice(0, limit);

console.log(output2);


let output3 = array1
  .filter((item, pos) => array1.indexOf(item) == pos)
  .sort((a, b) => a - b)
  .slice(0, limit);

console.log(output3);
