'use strict';

const obj = [
  {name: 'mark', age: 24},
  {name: 'nick', age: 34},
  {name: 'ken', age: 40},
  {name: 'eric', age: 19}
];

// adding new values to obj
let output1 = obj.map(function (o, i) {
  return {name: o.name, age: o.age + 1, gender: 'male'};
});

console.log(output1);
