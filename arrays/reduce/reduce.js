'use strict';

// Javascripts reduce method
// NOTE:: will combine all array values to one single value

let arr = ['camping', 'in', 'the', 'the', 'woods', 'is', 'fun'];

// finds how many characters in the array values
let output1 = arr.reduce(function (sum, item) {
  return sum + item.length;
}, 0);

console.log(output1);

// return string w/out duplicate values
let output2 = arr.reduce(function (sum, item) {
  if (sum.indexOf(item) === -1) {
    sum.push(item);
  }
  return sum
},[]).join(' ');

console.log(output2);

// average value in array
let arr2 = [1, 3, 5, 7, 9, 11, 13, 15, 17, 19];

let output3 = arr2.reduce(function (sum, item) {
  return sum + item;
}, 0) / arr2.length;

console.log(output3);
