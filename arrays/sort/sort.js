'use strict';

// Sorting arrays
// NOTE: the sort method can also be used for alphabetizing array values

let arr = [10, 4, 9, 2, 1];
let arr2 = ['zack', 'jen', 'jen', 'matt', 'nate'];

// organize int from greatest value to lowest value
let sort1 = arr.sort(function (a, b) {
  return a - b;
});

console.log(sort1);

// organize strings alpha
let sort2 = arr2.sort();

console.log(sort2);

// organize int from lowest value to greatest value
let sort3 = arr.sort(function (a, b) {
  return b - a;
});

console.log(sort3);

// reverse organize strings alpha
let sort4 = arr2.sort().reverse();

console.log(sort4);
