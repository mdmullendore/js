'use strict';

// Javascripts filter method
// NOTE:: arr.filter(callback(element[, index[, array]])[, thisArg]);

let arr = [6, 8, 8, 7, 9, 11, 24, 18, 36];

// Remove duplicate values
let output1 = arr.filter(function (item, pos) {
  return arr.indexOf(item) === pos;
});

console.log(output1);

// greater than x
let x = 10;
let output2 = arr.filter(function (a) {
  return a > x;
});

console.log(output2);

// less than y
let y = 10;
let output3 = arr.filter(function (a) {
  return a < y;
});

console.log(output3);

// divisible by int
let output4 = arr.filter(function (a) {
  return a % 3 == 0 && a % 6 == 0;
});

console.log(output4);

// string contains values
let fruits = ['apple', 'banana', 'grapes', 'mango', 'orange'];

let output5 = fruits.filter(function(item) {
  return item.toLowerCase().indexOf('an'.toLowerCase()) > -1;
});

console.log(output5);
