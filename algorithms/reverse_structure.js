'use stict';

// Problem
// Given a linked list of size N. The task is to reverse every k nodes (where k is an input to the function) in the linked list.
//
// Input:
// First line of input contains number of testcases T. For each testcase, first line contains length of linked list and next line contains the linked list elements.
//
// Output:
// For each testcase, there will be a single line of output which contains the linked list with every k group elements reversed.
//
// Example:
// Input:
// 1
// 8
// 1 2 2 4 5 6 7 8
// 4
//
// Output:
// 4 2 2 1 8 7 6 5
//
// Explanation:
// Testcase 2: Since, k = 2. So, we have to reverse everty group of two elements. Modified linked list is as 4, 2, 2, 1, 8, 7, 6, 5.

let x = 2;
let arr = [1, 2, 2, 4, 5, 6, 7, 8];
let section = arr.length / x;


let obj = arr.reverse().reduce(function (resultArray, item, index) {
  const sectionIndex = Math.floor(index/section);

  if (!resultArray[sectionIndex]) {
    resultArray[sectionIndex] = [];
  }

  resultArray[sectionIndex].push(item)

  return resultArray
}, []);


console.log(obj);
