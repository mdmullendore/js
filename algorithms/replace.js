'use strict';

// Replace all ‘0’ with ‘5’ in an input Integer
// Given a integer as a input and replace all the ‘0’ with ‘5’ in the integer.

let arr = [10, 1020, 1245, 3040];
let output = [];

arr.forEach(function (a) {
  if (a.toString().match(/0/gi)) {
    output.push(Number(a.toString().replace(/0/gi, 5)));
  } else {
    output.push(a);
  }
});

console.log(output);
